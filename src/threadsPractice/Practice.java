package threadsPractice;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Practice {

	public static void main(String[] args) {
		ScheduledExecutorService service = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
		
		Runnable count = new Runnable() {
			@Override
			public void run() {
				System.out.println("counting to 5 in 5 seconds");
				for(int i=1;i<=5;i++) {
					try {
						Thread.sleep(1000);   // sleep 1 second
					} catch (InterruptedException e) {
						e.printStackTrace();
					} 
					System.out.println(i);
				}
				System.out.println("go!");
			}	
		};
		Callable<Integer> run = new Callable<Integer>() {
			@Override
			public Integer call() throws Exception {
				System.out.println("task 2 started");
				int[] num = new Random().ints(5,0,1000).toArray();
				System.out.println("number generated: "+Arrays.toString(num));
				return Arrays.stream(num).sum();
			}
		};
		Future<?> result1 = service.schedule(count,0,TimeUnit.SECONDS);
		while(!(result1.isDone())){
		}
		System.out.println("task 1 finished, task 2 will start in 1.5 seconds");
		Future<Integer> result2 = service.schedule(run, 1500, TimeUnit.MILLISECONDS);
		try {
			System.out.println("sum of the numbers are: "+result2.get());
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		service.shutdownNow();
	}

}
