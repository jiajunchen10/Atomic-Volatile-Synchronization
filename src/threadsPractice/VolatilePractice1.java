package threadsPractice;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class VolatilePractice1 {

	public static volatile int count=100_000_000;
	// public volatile static int c = 0;   works
	// volatile public static int c = 0;     also works;
	// static public int c=0;              works
	//static public volatile int c = 0;    works
	
	
	// public volatile int fun() { };      volatile only works for variable
	
	public static void test() {
		Instant start = Instant.now();
		ExecutorService service  = Executors.newFixedThreadPool(2);
		service.execute(new Runnable() {
			@Override
			public void run() {
				System.out.println("thread 1 started with count = "+count);
				for(int i=0;i<count;count=count-1) { }
				System.out.println("thread 1 ended with count = "+count);
			}
		});
		service.execute(new Runnable() {
			@Override
			public void run() {
				System.out.println("thread 2 started with count = "+count);
				for(int i=0;i<count;count=count-1) { }
				System.out.println("thread 2 ended with count = "+count);
			}
		});
		service.shutdown();
		try {
			service.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Instant end = Instant.now();
		System.out.println("takes "+Duration.between(start, end).toMillis()+" milli-seconds");
	}
	public static void main(String[] args) {
		test();
	}

}
