package threadsPractice;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SynchronizedPractice {

	public static int count = 100_000_000;
	// static public synchronized void in() {}  works
	// synchronized static public void in() {}  works too
	public static synchronized int dec() {
		return count = count -1;
	}
	public static void test() {
		Instant start = Instant.now();
		ExecutorService service  = Executors.newFixedThreadPool(2);
		service.execute(new Runnable() {
			@Override
			public void run() {
				System.out.println("thread 1 started with count = "+count);
				for(int i=0;i<count;dec()) { }
				System.out.println("thread 1 ended with count = "+count);
			}
		});
		service.execute(new Runnable() {
			@Override
			public void run() {
				System.out.println("thread 2 started with count = "+count);
				for(int i=0;i<count;dec()) { }
				System.out.println("thread 2 ended with count = "+count);
			}
		});
		service.shutdown();
		try {
			service.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Instant end = Instant.now();
		System.out.println("takes "+Duration.between(start, end).toMillis()+" milli-seconds");
	}
	public static void main(String[] args) {
		System.out.println("===== Synchronized =======");
		test();
		System.out.println("===== volatile ========");
		VolatilePractice1.test();
	}
	
	
	/* ============================= conclusion ===============================
	 * SynchronizedPractice allows only one thread to access the code block or variable at a time.
	 * Volatile requires all threads to get the newest value of the volatile variable when every a thread is using it, because volatile variables are stored in the main memory instead of cache.
	 * Atomic variables allows programmers read and/or write to atomic variable at the same time.
	 */
}
