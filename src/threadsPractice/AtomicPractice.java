package threadsPractice;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicPractice {

	public static AtomicInteger count = new AtomicInteger(100_000_000);
	public static void main(String[] args) {
		Instant start = Instant.now();
		ExecutorService service  = Executors.newFixedThreadPool(2);
		service.execute(new Runnable() {
			@Override
			public void run() {
				System.out.println("thread 1 started with count = "+count.get());
				for(int i=0;i<count.get();count.decrementAndGet()) { }
				System.out.println("thread 1 ended with count = "+count.get());
			}
		});
		service.execute(new Runnable() {
			@Override
			public void run() {
				System.out.println("thread 2 started with count = "+count.get());
				for(int i=0;i<count.get();count.decrementAndGet()) { }
				System.out.println("thread 2 ended with count = "+count.get());
			}
		});
		service.shutdown();
		try {
			service.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Instant end = Instant.now();
		System.out.println("takes "+Duration.between(start, end).toMillis()+" milli-seconds");
	}
}
